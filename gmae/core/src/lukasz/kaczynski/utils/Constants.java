package lukasz.kaczynski.utils;

import com.badlogic.gdx.math.Vector2;

public class Constants {
    public static final int SCREEN_WIDTH=800;
    public static final int SCREEN_HEIGHT=600;
    public static final String LEVEL_FOLDER = "levels";

    public static final float TILE_SIZE=16;
    public static final float PIXELS_PER_MITER =100;

    public static final short BIT_GROUND =1;
    public static final short BIT_PLAYER =2;
    public static final short BIT_BRICK =4;
    public static final short BIT_COIN =8;
    public static final short BIT_DESTROYED =16;
    public static final short BIT_PLAYER_HEAD = 32;
}

