package lukasz.kaczynski.managers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import lukasz.kaczynski.screens.GameScreen;
import lukasz.kaczynski.sprites.entyties.PLayerOne;
import lukasz.kaczynski.sprites.entyties.Player;
import lukasz.kaczynski.sprites.interactive.tile.Brick;
import lukasz.kaczynski.utils.Constants;
import lukasz.kaczynski.utils.HelperClass;


/**
 * Class responsible for loading all elements of the current level
 */
public class LevelManager {

    public OrthogonalTiledMapRenderer mapRender;
    private TiledMap map;
    public World world;

    public Array<Brick> bricks;

    private GameScreen screen;
    private Box2DDebugRenderer b2dr;

    public Player player;

    public LevelManager(GameScreen screen) {
        this.screen = screen;
        b2dr = new Box2DDebugRenderer(); //shows the collision bodies
        world = new World(new Vector2(0, -10), true);// Gravity x, y, Allows the bodies sleeps
        bricks = new Array<Brick>();
    }

    /**
     * function responsible for loading the map
     *
     * @param levelName
     */
    public void loadCurrentMap(String levelName) {

        map = new TmxMapLoader().load(levelName);
        mapRender = new OrthogonalTiledMapRenderer(map, 1 / Constants.PIXELS_PER_MITER);

        screen.cameraManager.gamecam.position.set(screen.cameraManager.gamePort.getWorldWidth() / 2/Constants.PIXELS_PER_MITER, screen.cameraManager.gamePort.getWorldHeight() / 2/Constants.PIXELS_PER_MITER, 0);
        loadMap();
        world.setContactListener(new ContactManager());
    }

    /**
     * Load map objects
     */
    private void loadMap() {

        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        //GROUND
        for (MapObject object : map.getLayers().get("static").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) / Constants.PIXELS_PER_MITER,
                    (rect.getY() + rect.getHeight() / 2) / Constants.PIXELS_PER_MITER);

            body = world.createBody(bdef);
            shape.setAsBox((rect.getWidth() / 2) / Constants.PIXELS_PER_MITER, (rect.getHeight() / 2) / Constants.PIXELS_PER_MITER);
            fdef.shape = shape;
            body.createFixture(fdef);
        }

        //INTERACTIVE OBJECTS BRICKS
        for (MapObject object : map.getLayers().get("bricks").getObjects().getByType(RectangleMapObject.class)) {
            bricks.add(new Brick(map, world, ((RectangleMapObject) object).getRectangle()));
        }

        //CREATE PLAYER
        for (MapObject object : map.getLayers().get("player").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            player = new PLayerOne(screen, new Vector2(rect.x/Constants.PIXELS_PER_MITER, rect.y/Constants.PIXELS_PER_MITER));
        }

    }

    /**
     * Update the positions of the level elements such as the player, enemies, blocks and more
     *
     * @param dt
     */
    public void update(float dt) {
        world.step(1 / 60f, 6, 2);

        player.update(dt);
        screen.cameraManager.findPosition(player.body.getPosition().x, player.body.getPosition().y,dt, 10f );
        for (Brick obj : bricks) {
            if (obj.toDestroy) {
                obj.destroyObject();
                bricks.removeValue(obj, true);
            }
        }

        mapRender.setView(screen.cameraManager.gamecam);
    }


    /**
     * draws the map objects
     *
     * @param dt    float
     * @param batch SpriteBatch
     */
    public void draw(float dt, SpriteBatch batch) {
        //player.draw(batch);
        mapRender.render();
        b2dr.render(world, screen.cameraManager.gamecam.combined);
        //HelperClass.DrawDebugLine(new Vector2(0,0), player.body.getPosition(), screen.cameraManager.gamecam.combined);
    }

}
