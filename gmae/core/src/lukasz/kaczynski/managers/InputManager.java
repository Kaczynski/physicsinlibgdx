package lukasz.kaczynski.managers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

public class InputManager implements InputProcessor {
    public boolean active;
    private LevelManager levelManager;
    public InputManager(LevelManager levelManager) {
        super();
        this.levelManager = levelManager;
    }

    public boolean keyDown(int keycode) {
        levelManager.player.keyDownInput(keycode);
        return true;
    }

    public boolean keyUp(int keycode) {
        levelManager.player.keyUpInput(keycode);
        return true;
    }

    public boolean keyTyped(char character) {
        return true;
    }

    public boolean touchDown(int x, int y, int pointer, int button) {
        return false;
    }

    public boolean touchUp(int x, int y, int pointer, int button) {
        return false;
    }

    public boolean touchDragged(int x, int y, int pointer) {
        return false;
    }

    public boolean mouseMoved(int x, int y) {
        return false;
    }

    public boolean scrolled(int amount) {
        return false;
    }

}
