package lukasz.kaczynski.managers;

import com.badlogic.gdx.physics.box2d.*;
import lukasz.kaczynski.sprites.entyties.PLayerOne;
import lukasz.kaczynski.sprites.entyties.Player;
import lukasz.kaczynski.sprites.interactive.tile.InteractiveTileObject;
import lukasz.kaczynski.utils.Constants;

/**
 * Class responsible for processing the events produced by the
 * collisions between the different bodies shapes that exist in the world
 * A body can have several collision shapes (head body an legs for example) and can trigger different functions.
 */
public class ContactManager implements ContactListener {

    /**
     * Function activated when two bodies shapes collide
     *
     * @param contact Contact
     */
    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();
        int cDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;

        if (cDef == (Constants.BIT_BRICK|Constants.BIT_PLAYER_HEAD)) {
            Fixture head = fixA.getUserData() instanceof PLayerOne ? fixA : fixB;
            Fixture object = head == fixA ? fixB : fixA;

            if (object.getUserData() instanceof InteractiveTileObject) {
                ((InteractiveTileObject) object.getUserData()).onHeadHit();
            }
        }

        if (cDef == (Constants.BIT_GROUND | Constants.BIT_PLAYER)) {
            Fixture player = contact.getFixtureA().getUserData() instanceof PLayerOne ? contact.getFixtureA() : contact.getFixtureB();
            ((PLayerOne)player.getUserData()).setCanJump(true);
        }
        //System.out.println("fixture a: "+fixA.getFilterData().categoryBits+ ", Fixture b"+fixB.getFilterData().categoryBits);

    }

    /**
     * Function activated when two bodies shapes stop colliding collide
     *
     * @param contact
     */
    @Override
    public void endContact(Contact contact) {
        if ((contact.getFixtureA().getFilterData().categoryBits | contact.getFixtureB().getFilterData().categoryBits) == (Constants.BIT_GROUND | Constants.BIT_PLAYER)) {
            Fixture player = contact.getFixtureA().getUserData() instanceof PLayerOne ? contact.getFixtureA() : contact.getFixtureB();
            ((PLayerOne)player.getUserData()).setCanJump(false);
        }
    }

    /**
     * Function activated when two bodies shapes are going to collide
     *
     * @param contact
     * @param oldManifold
     */
    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    /**
     * Function activated when two bodies shapes stop colliding and the enContact is executed
     *
     * @param contact
     * @param impulse
     */
    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
