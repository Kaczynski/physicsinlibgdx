package lukasz.kaczynski.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import lukasz.kaczynski.PhysicsInLibgdx;
import lukasz.kaczynski.managers.CameraManager;
import lukasz.kaczynski.managers.InputManager;
import lukasz.kaczynski.managers.LevelManager;
import lukasz.kaczynski.utils.Constants;

public class GameScreen implements Screen {

    private PhysicsInLibgdx game;
    public CameraManager cameraManager;
    public LevelManager levelManager;


    public GameScreen(PhysicsInLibgdx game) {
        this.game = game;
        this.cameraManager = new CameraManager();
        this.levelManager = new LevelManager(this);
        levelManager.loadCurrentMap(Constants.LEVEL_FOLDER + "/ejemplo.tmx");

        //with  InputMultiplexer its posibble add more Imputs managers like stages and other stuff
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(new InputManager(levelManager));
        Gdx.input.setInputProcessor(multiplexer);

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        levelManager.update(delta);


        cameraManager.gamecam.update();

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.setProjectionMatrix(cameraManager.gamecam.combined);
        game.batch.begin();
        levelManager.draw(delta,game.batch);
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {
        cameraManager.gamePort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
