package lukasz.kaczynski.sprites.entyties;

import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import lukasz.kaczynski.managers.ResourceManager;
import lukasz.kaczynski.screens.GameScreen;
import lukasz.kaczynski.utils.Constants;

public abstract class Player extends Sprite {

    protected World world;

    public Body body;
    protected Vector2 position;
    protected Vector2 size;
    private GameScreen screen;


    /**
     * Class constructor
     * @param screen GameScreen
     * @param position Vector2
     */
    public Player(TextureAtlas.AtlasRegion texture, GameScreen screen, Vector2 position) {
        super(texture);
        this.world = screen.levelManager.world;
        this.screen = screen;
        this.position=position;
    }

    /**
     * Updates player
     * @param dt
     */
    public void update(float dt) {
        menageInput(dt);
        //check if the body is in the world bounds (for examle only in the left/bottom position)
        if (body.getPosition().x < 0) {
            body.setTransform(position,0);
        }
        if (body.getPosition().y < 0) {
            body.setTransform(position,0);
        }
    }

    /**
     *define all player fixtures and her body properties
     */
    protected void definePlayer() {


        //create a new body definition
        BodyDef bdef = new BodyDef();
        //define the initial position of the body
        bdef.position.set(position);
        bdef.type = BodyDef.BodyType.DynamicBody;

        //add the body to the world
        body = world.createBody(bdef);

        //create new fixture to body (coliders, and trigggers)
        FixtureDef fdef = new FixtureDef();

        //new shape colider (the base)
        CircleShape bodyShape = new CircleShape();
        bodyShape.setRadius(size.x/2 / Constants.PIXELS_PER_MITER);
        //relative tho te boy
        bodyShape.setPosition(new Vector2(0,0));
        //set the shape int the fixture
        fdef.shape = bodyShape;
        fdef.friction = 1f;
        //define the bits of fixture when colides with te staff in the world
        fdef.filter.categoryBits = Constants.BIT_PLAYER;

        defineFixtureDefBody(fdef);

        //create the fixture in the current body and add this object to fixture (for identify when collides with something)
        body.createFixture(fdef).setUserData(this);


        //it's possible to create other colliders from the same Fixture Definition and adds it to body
        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-10/Constants.PIXELS_PER_MITER, (size.y/2+2)/Constants.PIXELS_PER_MITER ), new Vector2(10/Constants.PIXELS_PER_MITER , (size.y/2+2) /Constants.PIXELS_PER_MITER));
        fdef.shape = head;
        fdef.filter.categoryBits = Constants.BIT_PLAYER_HEAD;
        body.createFixture(fdef).setUserData(this);
    }

    public abstract void menageInput(float dt);

    public abstract void keyDownInput(int keycode);

    public abstract void keyUpInput(int keycode);

    public abstract void mouseMovedInput(int x, int y);


    public abstract void defineFixtureDefBody(FixtureDef fixtureDef);

}
