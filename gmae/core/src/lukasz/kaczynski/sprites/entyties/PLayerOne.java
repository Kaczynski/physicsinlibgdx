package lukasz.kaczynski.sprites.entyties;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import lukasz.kaczynski.managers.ResourceManager;
import lukasz.kaczynski.screens.GameScreen;

import java.awt.*;

public class PLayerOne extends Player{

    private static final float JUMP_FORCE = 2f;
    private static final float ACELERATION = 0.2f;
    private boolean inputJump;
    private boolean inputRight;
    private boolean inputLeft;
    private boolean canJump = true;
    private static final Vector2 maxVelocity = new Vector2(2f,1f);


    public PLayerOne(GameScreen screen, Vector2 position) {
        super(ResourceManager.getAtlas("players/player_one.pack").findRegion("player_one_idle"),screen, position);
        size= new Vector2(32,32);
        definePlayer();
    }

    @Override
    public void menageInput(float dt) {

        if (inputJump  && canJump) {
            body.applyLinearImpulse(new Vector2(0, JUMP_FORCE), body.getWorldCenter(), true);
        }
        if (inputRight && body.getLinearVelocity().x <= maxVelocity.x) {
            body.applyLinearImpulse(new Vector2(ACELERATION, 0), body.getWorldCenter(), true);

        }
        if (inputLeft && body.getLinearVelocity().x >= -maxVelocity.x) {
            body.applyLinearImpulse(new Vector2(-ACELERATION, 0), body.getWorldCenter(), true);

        }
    }

    @Override
    public void defineFixtureDefBody(FixtureDef fixtureDef) {

    }

    @Override
    public void draw(Batch batch) {

    }

    @Override
    public void keyDownInput(int keycode) {

        switch (keycode)
        {
            case Input.Keys.A:
                inputLeft=true;
                break;
            case Input.Keys.D:
                inputRight=true;
                break;
            case Input.Keys.SPACE:
                //inputFire=true;
                break;
            case Input.Keys.W:
                inputJump=true;
                break;
        }
    }

    @Override
    public void keyUpInput(int keycode) {

        switch (keycode)
        {
            case Input.Keys.A:
                inputLeft=false;
                break;
            case Input.Keys.D:
                inputRight=false;
                break;
            case Input.Keys.SPACE:
                //inputFire=false;
                break;
            case Input.Keys.W:
                inputJump=false;
                break;
        }
    }

    @Override
    public void mouseMovedInput(int x, int y) {

    }

    public void setCanJump(boolean canJump) {
        this.canJump = canJump;
    }
}
