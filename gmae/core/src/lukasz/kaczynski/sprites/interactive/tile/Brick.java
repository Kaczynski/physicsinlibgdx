package lukasz.kaczynski.sprites.interactive.tile;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import lukasz.kaczynski.utils.Constants;

public class Brick extends InteractiveTileObject {
    public Brick(TiledMap map, World world, Rectangle bounds) {
        super(map, world, bounds);
        fixture.setUserData(this);
        setCategoryFilter(Constants.BIT_BRICK);
    }

    @Override
    public void onHeadHit() {
        setCategoryFilter(Constants.BIT_DESTROYED);
        getCell().setTile(null);
        toDestroy = true;

    }

}