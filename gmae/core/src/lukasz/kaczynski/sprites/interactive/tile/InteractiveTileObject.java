package lukasz.kaczynski.sprites.interactive.tile;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.*;
import lukasz.kaczynski.utils.Constants;

public abstract class InteractiveTileObject {

    protected TiledMap map;
    protected Rectangle bounds;
    protected Body body;
    protected Fixture fixture;
    protected World world;
    public boolean toDestroy;

    public InteractiveTileObject(TiledMap map, World world, Rectangle bounds) {
        this.map = map;
        this.bounds = bounds;
        this.world = world;
        toDestroy = false;

        BodyDef bdef = new BodyDef();
        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();

        bdef.type = BodyDef.BodyType.StaticBody;
        bdef.position.set((bounds.getX() + bounds.getWidth() / 2) / Constants.PIXELS_PER_MITER, (bounds.getY() + bounds.getHeight() / 2) / Constants.PIXELS_PER_MITER );
        body = world.createBody(bdef);

        shape.setAsBox(bounds.getWidth() / 2 / Constants.PIXELS_PER_MITER , bounds.getHeight() / 2  / Constants.PIXELS_PER_MITER);
        fdef.shape = shape;
        fixture = body.createFixture(fdef);

    }
    public abstract void onHeadHit();

    public void setCategoryFilter(short filterBit){
        Filter filter = new Filter();
        filter.categoryBits = filterBit;
        fixture.setFilterData(filter);
    }

    public TiledMapTileLayer.Cell getCell(){
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("bloques");
        return layer.getCell(((int)(body.getPosition().x*Constants.PIXELS_PER_MITER /Constants.TILE_SIZE)), ((int)(body.getPosition().y*Constants.PIXELS_PER_MITER /Constants.TILE_SIZE)));

    }

    public void destroyObject(){
        world.destroyBody(body);
    }
}
